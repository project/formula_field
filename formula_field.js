/**
 * @file
 */

(function ($) {
    Drupal.behaviors.formula_field = {
      attach: function (context, settings) {
          if ($("body").hasClass('formula-field-page')) {
              var formula_keys = [];
              if ($("#main_formulae").val() != '') {
                  formula_keys = JSON.parse($("#main_formulae").val());
              }
            $('.key').click(function (e) {
                e.preventDefault();
                var item = {};
                var formula_html = "";
                switch ($(this).data('function')) {
                    case "cancel":
                        formula_keys.pop(formula_keys)
                        break;
                    case "insert":
                        item.val = $(this).data('entry');
                        item.html = '<span class="item_' + $(this).data('type') + '">' + item.val + '</span>';
                        item.fieldname = $(this).data('fieldname');
                        item.type = $(this).data('type');
                        formula_keys.push(item);
                        break;
                    case "clear":
                        formula_keys = [];
                        break;
                    case "reset":
                        formula_keys = JSON.parse($("#main_formulae").data('default_formula'));
                        break;
                    case "submit":
                        break;
                }
                for (var i = 0; i < formula_keys.length; i++) {
                    formula_html += formula_keys[i].html + " ";
                };
                if (formula_keys.length == 0) {
                    formula_html = '<span class="item_default">** EMPTY **</span>';
                }
                $("#main_display").html(formula_html);
                // Feeding to main formulae.
                $("#main_formulae").val(JSON.stringify(formula_keys));
            });
          }
      }
    };
})(jQuery);
