--------------------------------
FORMULA FIELD
--------------------------------

It provides a new field type, which fetches all "integer" fields in a bundle (e.g. article)
and allows a user to create a formula, based on available fields for that particular bundle.
The value for this field is calculated based on the configured formula in the fields settings.

This module requires eval-math-master library, be sure to download it from URL below.

Place the library inside sites > all > libraries > eval-math-libraries.

DOWNLOAD ADDITIONAL LIBRARY
---------------------------
Composer/Packagist version of EvalMath by Miles Kaufman
Copyright (C) 2005 Miles Kaufmann <http://www.twmagic.com/>

NAME
----
    EvalMath - safely evaluate math expressions

DESCRIPTION
-----------
    Use the EvalMath class when you want to evaluate mathematical expressions
    from untrusted sources.  You can define your own variables and functions,
    which are stored in the object.  Try it, it's fun!

URL
---
    https://github.com/dbojdo/eval-math

NOTE
----
    Move the EvalMath library inside Drupal's library directory.
    Rename the library folder to "eval-math-master".
